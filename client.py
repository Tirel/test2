import socket

def connsocket(ip, port):
    sock = socket.socket()
    sock.connect((ip, port))
    return sock

def Get_a_unique_code():
    sock = connsocket('', 8001)
    login = input('Enter a unique identifier/login: ')
    sock.send(login.encode(encoding = 'UTF-8'))
    data = sock.recv(2048)
    print(f'Your unique identifier: {login}')
    print(f'Your unique code: {data.decode()}')
    sock.close()
    return (login, data.decode())

def send_message(login, message):
    sock = connsocket('', 8000)
    mssg = f'{login[0]}:{login[1]}:{message}'
    sock.send(mssg.encode(encoding = 'UTF-8'))
    data = sock.recv(2048)
    sock.close()
    return data.decode()

def menu():
    print(
'''
0) Get the code automatically
1) I have login and code
2) EXIT
'''
    )
    inp = input(':')
    if inp == '0':
        login = Get_a_unique_code()
        print(send_message(login, input('Enter message: ')))
    elif inp == '1':
        print(send_message((input('Enter a unique identifier/login: '), input('Enter a unique code: ')), input('Enter message: ')))
    elif inp == '2':
        pass

menu()