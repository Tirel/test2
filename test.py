import socket
import random
import string
import threading

def thread(my_func):
    def wrapper(*args, **kwargs):
        my_thread = threading.Thread(target=my_func, args=args, kwargs=kwargs)
        my_thread.start()
    return wrapper

logins = [''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(6, 20)) for i in range(0, 50)]
messages = [''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(1, 200)) for i in range(0, 200)]

def connsocket(ip, port):
    sock = socket.socket()
    sock.connect((ip, port))
    return sock

def Get_a_unique_code():
    sock = connsocket('', 8001)
    login = random.choice(logins)
    sock.send(login.encode(encoding = 'UTF-8'))
    data = sock.recv(2048)
    sock.close()
    return (login, data.decode())

@thread
def send_message(login, message):
    sock = connsocket('', 8000)
    mssg = f'{login[0]}:{login[1]}:{message}'
    sock.send(mssg.encode(encoding = 'UTF-8'))
    data = sock.recv(2048)
    sock.close()
    return data.decode()

for _ in messages:
    send_message(Get_a_unique_code(), random.choice(messages))