import socket
import random
import string
import threading
import datetime

USERS = {}

def thread(my_func):
    def wrapper(*args, **kwargs):
        my_thread = threading.Thread(target=my_func, args=args, kwargs=kwargs)
        my_thread.start()
    return wrapper

def cod_gen(login):
    if not login in USERS:
        cod = ''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(20))
        USERS[login] = cod
    else:
        cod = USERS[login]
    return cod

def log(data):
    listdata = data.split(':')
    if listdata[0] in USERS:
        open('log.txt', 'a').write(f'{datetime.datetime.now()}--{data}\n')
        return 'OK'
    return 'The message is not accepted'

@thread
def opensocket(ip, port, key_server=False):
    print(f'Server_UP {ip}:{port}')
    sock = socket.socket()
    sock.bind((ip, port))
    if key_server == False:
        while True:
            sock.listen(1)
            conn, addr = sock.accept()
            data = conn.recv(2048)
            conn.send(log(data.decode()).encode(encoding = 'UTF-8'))
    else:
        while True:
            sock.listen(1)
            conn, addr = sock.accept()
            data = conn.recv(2048)
            conn.send(cod_gen(data.decode()).encode(encoding = 'UTF-8'))

opensocket('127.0.0.1', 8000)
opensocket('127.0.0.1', 8001, key_server=True)